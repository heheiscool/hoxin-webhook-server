package com.bogo.webhook.service.impl;

import com.bogo.webhook.feign.TulingRobotService;
import com.bogo.webhook.feign.request.TulingRequest;
import com.bogo.webhook.feign.response.TulingResponse;
import com.bogo.webhook.service.RobotChatService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TulingChatServiceImpl implements RobotChatService {

    @Resource
    private TulingRobotService tulingRobotService;

    @Value("${tuling.app.key}")
    private String appKey;

    @Override
    public String get(String text, String userId) {
        TulingRequest request = TulingRequest.create(appKey,text,userId);
        TulingResponse response = tulingRobotService.get(request);
        return response.getText();
    }
}
