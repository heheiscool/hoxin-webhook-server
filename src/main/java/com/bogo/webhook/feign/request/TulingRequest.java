package com.bogo.webhook.feign.request;

import java.util.HashMap;
import java.util.Map;

public class TulingRequest {

    private final int reqType = 0;

    private final Map<String,Object> perception = new HashMap<>();


    private final Map<String,Object> userInfo = new HashMap<>();

    public static TulingRequest create(String appKey,String text,String uid){
        TulingRequest request = new TulingRequest();

        Map<String,String> input = new HashMap<>();
        input.put("text",text);
        request.perception.put("inputText",input);
        request.userInfo.put("apiKey",appKey);
        request.userInfo.put("userId",uid);
        return request;
    }


    public int getReqType() {
        return reqType;
    }

    public Map<String, Object> getPerception() {
        return perception;
    }

    public Map<String, Object> getUserInfo() {
        return userInfo;
    }
}
