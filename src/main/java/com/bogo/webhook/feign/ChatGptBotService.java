
package com.bogo.webhook.feign;

import com.bogo.webhook.feign.request.OpenAIRequest;
import com.bogo.webhook.feign.response.OpenAIResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@FeignClient(value = "chat-gpt-service",url = "https://api.openai.com")
public interface ChatGptBotService {

	@PostMapping(path = "/v1/chat/completions",headers = {"Authorization=Bearer ${openai.app.key}"})
	OpenAIResponse get(@RequestBody OpenAIRequest request);
}
