
package com.bogo.webhook.feign;

import com.bogo.webhook.feign.request.RobotMessageRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "bogo-service",url = "${bogo.server.uri}")
public interface BogoRobotService {

	@PostMapping(path = "/open/robot/message")
	void send(@RequestParam String uuid, @RequestBody RobotMessageRequest request);
}
