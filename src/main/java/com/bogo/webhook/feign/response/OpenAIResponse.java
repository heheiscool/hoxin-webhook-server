package com.bogo.webhook.feign.response;

import java.util.List;
import java.util.Map;

public class OpenAIResponse {

    private List<Choice> choices ;

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public String getText(){

        if (choices == null || choices.isEmpty()){
            return null;
        }

        String text = choices.get(0).getMessage().content;

        return text == null ? null : text.replaceFirst("\n\n","");
    }

    public static class Choice{
        private Message message;

        public Message getMessage() {
            return message;
        }

        public void setMessage(Message message) {
            this.message = message;
        }
    }

    public static class Message{
        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        private String content;

    }
}
