package com.bogo.webhook.feign.response;

import java.util.List;
import java.util.Map;

public class TulingResponse {

    private List<Map<String,Object>> results ;

    public List<Map<String, Object>> getResults() {
        return results;
    }

    public void setResults(List<Map<String, Object>> results) {
        this.results = results;
    }

    public String getText(){

        if (results == null || results.isEmpty()){
            return null;
        }

        Map<String, Object> data = results.get(0);

        Map<String, String> values = (Map<String, String>) data.get("values");

        return values.get("text");
    }
}
